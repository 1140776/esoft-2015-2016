package gestaoexposicoes.states;

import gestaoexposicoes.model.Exposicao;
/**
 *
 * @author Barros
 */
public abstract class ExposicaoEstadoCriado implements ExposicaoState{

    /**
     * Exposicao
     */
    private Exposicao exposicao;

    /**
     * Criar uma instância EventoEstadoImplementado que recebe como parâmetro o
     * evento
     *
     * @param e evento
     */
    public ExposicaoEstadoCriado(Exposicao e) {
        exposicao = e;
    }

    /**
     * Devolve evento
     *
     * @return evento
     */
    public Exposicao getEvento() {
        return this.exposicao;
    }

    /**
     * Evento no estado registado
     *
     * @return resposta estado
     */
    @Override
    public boolean emRegistado() {
        return false;
    }

    /**
     * Evento no estado Sessões Temáticas Definidas
     *
     * @return resposta estado
     */
    @Override
    public boolean emSTDefinidas() {
        return false;
    }

    /**
     * Evento no estado Comissão de Programa Definida
     *
     * @return resposta estado
     */
    @Override
    public boolean emCPDefinida() {
        return false;
    }

    /**
     * Evento em submissão
     *
     * @return resposta estado
     */
    @Override
    public boolean emSubmissao() {
        return false;
    }

    /**
     * Evento em deteção de conflitos
     *
     * @return resposta estado
     */
    @Override
    public boolean emDetecaoConflitos() {
        return false;
    }

    /**
     * Evento em distribuição
     *
     * @return resposta estado
     */
    @Override
    public boolean emDistribuicao() {
        return false;
    }

    /**
     * Evento em revisão
     *
     * @return resposta estado
     */
    @Override
    public boolean emRevisao() {
        return false;
    }

    /**
     * Evento em decisão
     *
     * @return resposta estado
     */
    @Override
    public boolean emDecisao() {
        return false;
    }

    /**
     * Evento no estado decidido
     *
     * @return resposta estado
     */
    @Override
    public boolean emDecidido() {
        return false;
    }

    /**
     * Evento no estado cameraReady
     *
     * @return resposta estado
     */
    @Override
    public boolean emCameraReady() {
        return false;
    }

    /**
     * Modifica estado do evento para registado
     *
     * @return resposta estado
     */
    @Override
    public boolean setEstadoRegistado() {
        return false;
    }

    /**
     * Modifica estado do evento para submissão
     *
     * @return resposta estado
     */
    @Override
    public boolean setEstadoEmSubmissao() {
        return false;
    }

    /**
     * Modifica estado do evento para deteção de conflitos
     *
     * @return resposta estado
     */
    @Override
    public boolean setEstadoEmDetecaoConflitos() {
        return false;
    }

    /**
     * Modifica estado do evento para distibuição
     *
     * @return resposta estado
     */
    @Override
    public boolean setEstadoEmDistribuicao() {
        return false;
    }


    /**
     * Modifica estado do evento para estado decisão
     *
     * @return resposta estado
     */
    @Override
    public boolean setEstadoEmDecisao() {
        return false;
    }

    /**
     * Modifica estado do evento para estado decidido
     *
     * @return resposta estado
     */
    @Override
    public boolean setEstadoEmDecidido() {
        return false;
    }

}
