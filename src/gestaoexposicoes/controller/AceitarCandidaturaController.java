/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.Candidatura;
import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.Exposicao;
import gestaoexposicoes.model.Organizador;
import gestaoexposicoes.model.RegistoStands;
import gestaoexposicoes.model.Stand;
import java.util.ArrayList;

/**
 *
 * @author Pedro Couto <1140541@isep.ipp.pt>
 */
public class AceitarCandidaturaController {

    private Candidatura c;
    private CentroExposicoes centro;

    public ArrayList<Exposicao> getExposicoesOrganizador(Organizador o) {
        return centro.getExposicoesOrganizador(o);

    }

    public ArrayList<Candidatura> getListaCandPorAceitar(Exposicao e) {
        return e.getListaCandPorAceitar();

    }

    /**
     * @param c the c to set
     */
    public void setCandidatura(Candidatura c) {
        this.c = c;
    }

    /**
     * @param c the c to set
     */
    public void setDecisao(boolean d) {
        c.setDecisao(d);
    }

    public RegistoStands getRegistoStandsLivres() {
        return centro.getRegistoStandsLivres();
    }

    public void setStand(Stand s) {
        c.setStand(s);
        centro.removerStandLivre(s);
        centro.addStandUsado(s);
    }
     public void setRejeitada() {
        c.setRejeitada();
      
    }
}
