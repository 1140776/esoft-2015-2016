/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.Stand;
import java.util.Calendar;

/**
 *
 * @author Pedro Couto <1140541@isep.ipp.pt>
 */
public class DefinirStandCentroController {

    private Stand stand;
    private String titulo;
    private String des;
    private Calendar dataIn;
    private Calendar dataFim;
    private final CentroExposicoes c;

    public DefinirStandCentroController(CentroExposicoes c) {
        this.c=c;
    }
    
    public void inserirDados(String titulo, String des, Calendar dataIn, Calendar dataFim) {
        stand = new Stand();
        setTitulo(titulo);
        setDes(des);
        setDataIn(dataIn);
        setDataFim(dataFim);
        
    }
    
    public boolean valida() {
        // Introduzir as validações aqui
        return true;
    }
 public void registarStand() {
    c.getRegistoStandsLivres().addStandLivre(stand);
    
    }
    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @param des the des to set
     */
    public void setDes(String des) {
        this.des = des;
    }

    /**
     * @param dataIn the dataIn to set
     */
    public void setDataIn(Calendar dataIn) {
        this.dataIn = dataIn;
    }

    /**
     * @param dataFim the dataFim to set
     */
    public void setDataFim(Calendar dataFim) {
        this.dataFim = dataFim;
    }
}
