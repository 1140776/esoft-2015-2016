/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.*;
import java.util.List;

/**
 *
 * @author Luís Maia
 */
public class AtribuirCandidaturasController {

    private final CentroExposicoes centroExposicoes;
    private Exposicao exposicao;
    private MecanismoAtribuicao mecanismoAtribuicao;

    public AtribuirCandidaturasController(CentroExposicoes centroExposicoes) {
        this.centroExposicoes = centroExposicoes;

    }

    public List<Exposicao> getListaExposicoes(String username) {
        return centroExposicoes.getlExposicoes().getExposicaoOrganizador(username);
    }

    public void setExposicao(Exposicao e) {
        this.exposicao = e;
    }

    public void addCandidatura(Candidatura candidatura) {
        exposicao.addCandidatura(candidatura);
    }

    public List<MecanismoAtribuicao> getMecanismosDistribuicao() {
        return centroExposicoes.getMecanismosAtribuicao();
    }

    public MecanismoAtribuicao setMecanismosAtribuicao(MecanismoAtribuicao mecAtrib) {
        return this.mecanismoAtribuicao = mecAtrib;
    }

}
