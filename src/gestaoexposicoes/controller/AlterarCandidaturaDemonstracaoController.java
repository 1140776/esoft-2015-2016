/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.controller;

import gestaoexposicoes.model.Candidatura;
import gestaoexposicoes.model.CandidaturaDemonstracao;
import gestaoexposicoes.model.CentroExposicoes;
import gestaoexposicoes.model.Demonstracao;
import gestaoexposicoes.model.Representante;
import java.util.ArrayList;

/**
 *
 * @author Pedro Couto <1140541@isep.ipp.pt>
 */
public class AlterarCandidaturaDemonstracaoController {
        private Candidatura c;
    private CandidaturaDemonstracao cd;
    private CentroExposicoes centro;
    private Demonstracao d;
    private String nomeEmpresa;
    private String morada;
    private int tlm;
    
       public ArrayList<CandidaturaDemonstracao> getListaCandidaturaDemonstracao(Representante r) {
        return centro.getListaCandidaturaDemonstracao(r);
    }

    /**
     * @param cd the cd to set
     */
    public void setCandidatura(CandidaturaDemonstracao cd) {
        this.cd = cd;
    }
     
     public void setDados(String nomeEmpresa, String morada, int tlm) {
        this.nomeEmpresa=nomeEmpresa;
        this.morada=morada;
        this.tlm=tlm;
    }
       
     public void confirmarAlteracao(String nomeEmpresa, String morada, int tlm) {
      
        cd.setNomeEmpresa(nomeEmpresa);
        cd.setMorada(morada);
        cd.setTlm(tlm);
        cd.valida();
    }
       
}
