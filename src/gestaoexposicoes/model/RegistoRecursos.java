/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Luís Maia
 */
public class RegistoRecursos {

    private final List<Recurso> lstRecurso;

    public RegistoRecursos() {
        this.lstRecurso = new ArrayList<>();
    }

    public List<Recurso> getRegistoRecurso() {
        return this.lstRecurso;
    }

    public Recurso novoRecurso(String designacao) {
        return new Recurso(designacao);
    }

    public  void addRecurso(Recurso r) {
        lstRecurso.add(r);
    }

    public boolean registaRecurso(Recurso r) {
        if (validaRecurso(r)) {
            addRecurso(r);
            return true;
        }
        return false;
    }

    private boolean validaRecurso(Recurso r) {
        return !lstRecurso.contains(r);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegistoRecursos other = (RegistoRecursos) obj;
        if (!Objects.equals(this.lstRecurso, other.lstRecurso)) {
            return false;
        }
        return true;
    }

}
