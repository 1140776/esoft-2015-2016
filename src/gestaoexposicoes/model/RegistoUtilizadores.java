/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luís Maia
 */
public class RegistoUtilizadores {

    private final List<Utilizador> m_lUtilizadores;

    public RegistoUtilizadores() {
        this.m_lUtilizadores = new ArrayList<>();
    }

    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    public boolean registaUtilizador(Utilizador u) {
        if (validaUtilizador(u)) {
            addUtilizador(u);
            return true;
        }
        return false;
    }

    private boolean validaUtilizador(Utilizador u) {
        return !m_lUtilizadores.contains(u);
    }

    private void addUtilizador(Utilizador u) {
        m_lUtilizadores.add(u);
    }

    public List<Utilizador> getUtilizadores() {
        return this.m_lUtilizadores;
    }

    public Utilizador getUtilizador(String strId) {
        for (Utilizador u : this.m_lUtilizadores) {
            if (u.hasID(strId)) {
                return u;
            }
        }
        return null;
    }

    public List<Utilizador> getUtilizadoresNaoRegistados() {
        List<Utilizador> lUsers = new ArrayList<Utilizador>();

        for (Utilizador u : m_lUtilizadores) {
            if (!u.getRegistado()) {
                lUsers.add(u);
            }
        }
        return lUsers;
    }

    public Utilizador getUtilizadorInfo(String uId) {
        for (Utilizador u : m_lUtilizadores) {
            if (uId.equalsIgnoreCase(u.getUsername())) {
                return u;
            }
        }
        return null;
    }

    public void confirmaRegistoUtilizador(Utilizador u) {
        u.setRegistado(true);
    }
}
