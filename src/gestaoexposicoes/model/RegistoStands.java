/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pedro Couto <1140541@isep.ipp.pt>
 */
public class RegistoStands {

    private final List<Stand> lstStand;

    public void addStandLivre(Stand stand) {
        valida();
        addStand(stand);
    }

    public RegistoStands() {
        this.lstStand = new ArrayList<>();
    }

    private boolean valida() {
        return true;
    }

    public void addStand(Stand s) {
        lstStand.add(s);
    }
}
