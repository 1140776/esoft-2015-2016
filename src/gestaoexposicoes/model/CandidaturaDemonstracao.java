/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

/**
 *
 * @author Pedro Couto <1140541@isep.ipp.pt>
 */
public class CandidaturaDemonstracao {

    private String nomeEmpresa;

    private String morada;
    private int tlm;

    public CandidaturaDemonstracao() {
    }

    /**
     * @param nomeEmpresa the nomeEmpresa to set
     */
    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    /**
     * @param morada the morada to set
     */
    public void setMorada(String morada) {
        this.morada = morada;
    }

    /**
     * @param tlm the tlm to set
     */
    public void setTlm(int tlm) {
        this.tlm = tlm;
    }

    public boolean valida() {
        return true;
    }
}
