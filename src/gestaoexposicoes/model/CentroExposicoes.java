/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class CentroExposicoes {

    private final RegistoStands rsl;
    private final RegistoUtilizadores m_lUtilizadores;
    private final RegistoExposicao m_lExposicoes;
    private final RegistoRecursos registoRecurso;
    private List<MecanismoAtribuicao> mecanismosAtribuicao;
    private List<MecanismoDetecaoConflitos> mecanismoDetecaoConflitoses;
    private Timer timer;

    public CentroExposicoes(RegistoExposicao re, RegistoUtilizadores ru, RegistoRecursos rr, RegistoStands rs) {
        this.m_lUtilizadores = ru;
        this.m_lExposicoes = re;
        this.registoRecurso = rr;
        this.rsl = rs;
        this.mecanismosAtribuicao = new ArrayList<>();
        this.mecanismoDetecaoConflitoses = new ArrayList<>();
        this.timer = new Timer();
        fillInData();
    }

    public RegistoUtilizadores getlUtilizadores() {
        return m_lUtilizadores;
    }

    public RegistoExposicao getlExposicoes() {
        return m_lExposicoes;
    }

    public RegistoRecursos getlRecurso() {
        return registoRecurso;
    }

    public List<Recurso> getRegistoRecursos() {
        return this.registoRecurso.getRegistoRecurso();
    }

    public List<Utilizador> getRegistoUtilizadores() {
        return this.m_lUtilizadores.getUtilizadores();
    }

    public List<Exposicao> getRegistoExposicao() {
        return this.m_lExposicoes.getM_lExposicoes();
    }

    private void fillInData() {
        // Dados de Teste
        //Preenche alguns utilizadore
        for (Integer i = 1; i <= 4; i++) {
            getRegistoUtilizadores().add(new Utilizador("Utilizador " + i.toString(), "mail" + i.toString() + "@exposicoes.pt", Boolean.TRUE));
        }
    }

    public void schedule(TimerTask task, Calendar data) {
        Date time = data.getTime();
        timer.schedule(task, time);
    }

    public List<MecanismoAtribuicao> getMecanismosAtribuicao() {
        return mecanismosAtribuicao;
    }

    public void setMecanismosAtribuicao(List<MecanismoAtribuicao> mecanismosAtribuicao) {
        this.mecanismosAtribuicao = mecanismosAtribuicao;
    }

    public RegistoTipoConflito getRegistoTipoConflito() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public RegistoConflitos getRegistoConflitos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public RegistoStands getRegistoStandsLivres() {
        return rsl;
    }

    public ArrayList<Exposicao> getExposicoesOrganizador(Organizador o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void removerStandLivre(Stand s) {
        //      
    }

    public void addStandUsado(Stand s) {
        //      
    }

    public ArrayList<Candidatura> getCandidaturasAceites(Representante r) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ListaCandidaturas getListaCandidatura(Representante r) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<CandidaturaDemonstracao>  getListaCandidaturaDemonstracao(Representante r) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
