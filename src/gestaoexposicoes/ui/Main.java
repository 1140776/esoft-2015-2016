/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes.ui;

import gestaoexposicoes.model.*;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            RegistoUtilizadores ru = new RegistoUtilizadores();
            RegistoExposicao re = new RegistoExposicao();
            RegistoRecursos rr = new RegistoRecursos();
            RegistoStands rs = new RegistoStands();
            CentroExposicoes ce = new CentroExposicoes(re,ru,rr, rs);

            MenuUI uiMenu = new MenuUI(ce, re, ru, rr);

            uiMenu.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
